USE companyHR;

# CONCAT() - takes inputs and joins them together
SELECT CONCAT('Hello', ' World'); -- output: 'Hello World'
SELECT CONCAT(1, 2); -- output: 12
SELECT  CONCAT('3', ' INPUT', ' VALUES'); -- output: '3 INPUT VALUES'

# SUBSTRING() - takes a string, starting positon of substring (index starts at 1), [optional]span of substring
SELECT SUBSTRING('Parth Shreyash Patel', 7, 8); -- outputs: Shreyash

# Time Based Functions
SELECT NOW(); -- outputs the current date and time
SELECT CURDATE(); -- outputs current date
SELECT CURTIME(); -- outputs current time

# AGGREGATE FUNCTIONS
	-- COUNT()
SELECT COUNT(*) FROM employees; -- passing '*' returns number of rows
SELECT COUNT(contact_number) FROM employees; -- passing column_name returns number of values (non-NULL)
SELECT COUNT(DISTINCT gender) FROM employees; -- 'DISTINCT' keyword gets rid of duplicates
	-- AVG()
SELECT AVG(salary) FROM employees; -- returns average from the column
SELECT ROUND(AVG(salary), 2) FROM employees; -- ROUND() takes number to round and the amount of decimal places
	-- MAX(), MIN(), SUM()
SELECT MAX(salary) FROM employees; -- returns largest value from set
SELECT MIN(salary) FROM employees; -- returns lowest value from set
SELECT SUM(salary) FROM employees; -- returns sum of values from set
SELECT MAX(salary) FROM employees WHERE gender='M';
	-- GROUP BY
SELECT gender, MAX(salary) FROM employees GROUP BY gender;
	-- HAVING
SELECT gender, MAX(salary) FROM employees GROUP BY gender HAVING MAX(salary) > 10000;

# JOINS and UNION
SELECT employees.id, employees.em_name AS 'Mentor', mentorships.project AS 'Projects'
	FROM employees JOIN mentorships ON employees.id = mentorships.mentor_id;

SELECT em_name, salary FROM employees WHERE gender = 'M'
	UNION
SELECT em_name, years_in_company FROM employees WHERE gender = 'F';

SELECT mentor_id FROM mentorships
	UNION ALL
SELECT id FROM employees WHERE gender = 'F';
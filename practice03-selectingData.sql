USE companyHR;
# Aliases
SELECT em_name, gender FROM employees;
SELECT em_name AS 'Employee Name', gender AS Gender FROM employees;
# LIMITING RESULTS
SELECT em_name AS 'Employee Name', gender AS Gender FROM employees LIMIT 3;
# DISTINCT
SELECT gender FROM employees;
SELECT DISTINCT(gender) FROM employees;
# COMPARISON OPERATORS (=, !=, >, >=, <, <=)
SELECT * FROM employees WHERE id = 1;
SELECT * FROM employees WHERE id >= 3;
# Below, both produce same result
SELECT * FROM employees WHERE id >= 3 && id <= 7; -- '&& AND ||' will be deprecated DO NOT USE;
SELECT * FROM employees WHERE id between 3 and 7;
# PATTERN MATCHING (kind of like regex)
SELECT * FROM employees WHERE em_name LIKE '%er'; -- '%' means any number of characters;
SELECT * FROM employees WHERE em_name LIKE '%er %';
SELECT * FROM employees WHERE em_name LIKE '____e%'; -- '_' represents exactly one character;
# IN and NOT IN
SELECT * FROM employees WHERE id IN (6, 7, 9);
SELECT * FROM employees WHERE id NOT IN (6, 7, 9);
# LOGICAL OPERATORS
SELECT * FROM employees WHERE gender = 'F' AND (years_in_company > 5 OR salary > 5000);
# Subqueries
SELECT * FROM employees WHERE id IN (SELECT mentor_id FROM mentorships WHERE project='SQF Limited');
	-- gets 'id' values from mentorships table first then passes that as argument into main query
# Sorting Rows
SELECT * FROM employees ORDER BY gender, salary; -- sorts by default in ascending order;
SELECT * FROM employees ORDER BY gender, salary DESC; -- add 'DESC' to get descending order;

